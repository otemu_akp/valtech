//js functionality
(function($){
	$(function(){
		//click handler
		$('.sidebar-btn').on('click', function(){
			$(this).toggleClass("active");
			$('.sidebar').fadeToggle( "slow", "linear" );
		})
	})
})(jQuery);