var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var sourcemaps      = require('gulp-sourcemaps');
var filter        = require('gulp-filter');
var nunjucksRender = require('gulp-nunjucks-render');
var browserSync     = require('browser-sync').create();
var reload        = browserSync.reload;
var plumber       = require('gulp-plumber');
var sass = require('gulp-sass');
var uglify              = require('gulp-uglify');
var concat              = require('gulp-concat');

/* task for sass */
gulp.task('sass', function() {

  // Sass task error handler
  var errorHandler = function(errorObj) {
    // Notify the user
    browserSync.notify('Error: ' + beautifyMessage(errorObj.message));

    // Post the message in the console
    console.log(errorObj.message);

    // End this task
    this.emit('end');
  };

  var pipeErrorHandler = plumber(errorHandler);

  var task = gulp.src('src/scss/test.scss')
    .pipe(pipeErrorHandler)
    .pipe(sourcemaps.init()) 
    .pipe($.sass({
      outputStyle: 'compressed' // if css compressed **file size**
    }).on('error', $.sass.logError))
    .pipe(sourcemaps.write( '.' ))  
    .pipe(gulp.dest('dist/css'))
    .pipe(filter('**/*.css'))               // filter only css files (remove the map file)
    .pipe(reload({stream: true}));            // inject the changed css

  return task;
});

/* task for nunjucks */
gulp.task('nunjucks', function() {
  // Gets .html and .nunjucks files in pages
  return gulp.src('src/html/pages/**/*.+(html|nunjucks)')
  // Renders template with nunjucks
  .pipe(nunjucksRender({
      path: ['src/html/templates']
    }))
  // output files in app folder
  .pipe(gulp.dest(''))
  .pipe(reload({stream: true}));  
});

// js
gulp.task('compress', function() {
  return gulp.src(['./src/js/test.js'])
    .pipe(uglify())
    .pipe(concat('test.js'))
    .pipe(gulp.dest('./dist/js'));
});

// Browser sync server
gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
        baseDir: "./"
    }
  });
});

// Watch
gulp.task('watch', function() {
  gulp.watch(['src/scss/**/*.scss'], ['sass']);
  //gulp.watch('src/**/*{.shtml,.html}').on('change', reload);
  gulp.watch(['src/html/templates/**/*.html', 'src/html/pages/**/*.html'], ['nunjucks']);
  // JS watch
  gulp.watch('src/js/*.js', ['compress', reload]);
  
});

// Defaut Task
gulp.task('default', ['browser-sync', 'sass', 'compress', 'watch']);

// Helpers

/**
 * Prepare message for browser notify.
 * @param  {string} message raw message
 * @return {string}         parsed message - new lines replaced by html elements.
 */
function beautifyMessage(message) {
  return '<p style="text-align: left">' + message.replace(/\n/g, '<br>') + '</p>';
};

